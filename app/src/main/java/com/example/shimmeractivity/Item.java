package com.example.shimmeractivity;

import android.widget.ImageView;

/**
 * Created by Dhimas Saputra on 20/10/20
 * Jakarta, Indonesia.
 */
public class Item {
    ImageView imageView;
    String title;
    String desc;

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
